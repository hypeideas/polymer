/*
	Copyright 2015, Google, Inc. 
 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 
  
    http://www.apache.org/licenses/LICENSE-2.0 
  
 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
*/
"use strict";

var compress = require('compression'),
	express = require('express'),
	//https://www.npmjs.com/package/connect-modrewrite
	modRewrite = require('connect-modrewrite'),
	app = express();

//------------------------ CONFIGURACIÓN DE EXPRESS ------------------------------------


/* Include the app engine handlers to respond to start, stop, and health checks. */
app.use(require('./lib/appengine-handlers'));
app.use(compress());

var oneDay = 86400000,
	oneWeek = oneDay * 7,
	googleSites = 'https://sites.google.com/a/patakki.com/bogotatimbafest/';

app.set('port', (process.env.PORT || 5000) );
app.use(express.static(__dirname + '/public', { maxAge: oneDay / 2 }));
app.use(express.static('googleSites', { maxAge: oneDay }));

app.use( modRewrite([ '^/index\.html$ - [L]',
					  '^/sitemap\.xml$ /sitemap.xml [NC,L]'
					]),
					function(request, response)
					{
					 //response.sendfile('./public/index.html');
					 	response.set('Content-Type', 'text/plain');

					});	


app.use(function (req, res, next) 
		{

	res.setHeader('Expires', new Date( Date.now() + oneDay ).toUTCString() );
});


var allowCrossDomain = function(req, res, next) 
				   		{									
							res.header('Access-Control-Allow-Credentials', true);
					    	res.header('Access-Control-Allow-Origin', '*');
					    	res.header('Access-Control-Allow-Methods', 'GET,POST');
					    	res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');
						      
						    // intercept OPTIONS method
						    if ('OPTIONS' == req.method) {
						      res.send(200);
						    }
						    else {
						      next();
						    }

						};

app.use( allowCrossDomain );		

app.listen(app.get('port'), function() {
  console.log("Node app is running at localhost:" + app.get('port'))

})

